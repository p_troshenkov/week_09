package ru.edu.task1.xml;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * ReadOnly
 */
public class AppXMLTest {

    @Test
    public void run() {
        assertTrue(AppXML.run().isValid());
    }
}