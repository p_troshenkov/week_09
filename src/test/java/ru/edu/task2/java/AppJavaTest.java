package ru.edu.task2.java;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * ReadOnly
 */
public class AppJavaTest {

    @Test
    public void run() {
        assertTrue(AppJava.run().isValid());
    }
}