package ru.edu.task2.java;

import jdk.nashorn.internal.objects.annotations.Property;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

/**
 * Класс для настройки контекста контейнера зависимостей.
 */
public class AppJava {

    public static MainContainer run(){
        ApplicationContext context = new AnnotationConfigApplicationContext(AppJava.class);
        return context.getBean(MainContainer.class);
    }

    @Bean
    @Scope("prototype")
    public TimeKeeper timeKeeper() {
        return new TimeKeeper();
    }

    @Bean
    public Child child() {
        return new Child();
    }

    @Bean
    public MainContainer mainContainer() {
        return new MainContainer(new TimeKeeper(), new Child());
    }
}
