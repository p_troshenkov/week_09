package ru.edu.task2.java;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
public class Child {

    @Autowired
    private TimeKeeper timeKeeper;

    public TimeKeeper getTimeKeeper() {
        return timeKeeper;
    }
}
