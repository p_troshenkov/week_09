package ru.edu.task1.xml;

/**
 * ReadOnly
 */
public class ComponentC {

    private boolean isInit;


    public void init() {
        isInit = true;
    }


    public boolean isValid() {
        return isInit;
    }
}
