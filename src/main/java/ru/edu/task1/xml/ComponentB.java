package ru.edu.task1.xml;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

/**
 * ReadOnly
 */
@PropertySource("classpath:/task1.properties")
public class ComponentB {

    private String string;

    public ComponentB(
            @Value("${component_b.default.string}")
                    String string
    ) {
        this.string = string;
    }

    public boolean isValid() {
        return "stringForComponentB".equals(string);
    }
}
