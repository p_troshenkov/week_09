package ru.edu.task1.java;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;

/**
 * Класс для настройки контекста контейнера зависимостей.
 */
@Configuration
@PropertySource("classpath:task1.properties")
public class AppJava {

    @Value("${component_b.default.string}")
    private String stringForComponentB;

    public static MainContainer run() {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppJava.class);
        return context.getBean(MainContainer.class);
    }

    @Bean
    public ComponentA componentA() {
        return new ComponentA(new ComponentC());
    }

    @Bean
    public ComponentB componentB() {
        return new ComponentB(stringForComponentB);
    }

    @Bean (initMethod = "init")
    public ComponentC componentC() {
        return new ComponentC();
    }

    @Bean
    public MainContainer mainContainer() {
        return new MainContainer();
    }
}
