package ru.edu.task5.common;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName)
            throws BeansException {
        if (bean instanceof InterfaceToWrap) {
            return (InterfaceToWrap) () -> ("wrapped " + beanName);
        }

        if (bean instanceof InterfaceToRemove) {
            return "";
        }

        return bean;
    }

}
