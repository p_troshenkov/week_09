package ru.edu.task3.xml;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * ReadOnly
 */
public class AppXML {
    public static MainContainer run(String profile) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("task_03.xml");
        context.getEnvironment().setActiveProfiles(profile);
        context.refresh();
        return context.getBean(MainContainer.class);
    }
}
