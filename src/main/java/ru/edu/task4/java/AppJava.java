package ru.edu.task4.java;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import ru.edu.task2.java.Child;
import ru.edu.task2.java.TimeKeeper;

/**
 * Класс для настройки контекста контейнера зависимостей.
 */
public class AppJava {

    public static MainContainer run(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppJava.class);
        return context.getBean(MainContainer.class);
    }

    @Bean
    public CacheService cacheService() {
        return new CacheService(new RealService());
    }

    @Bean
    public RealService realService() {
        return new RealService();
    }

    @Bean
    public MainContainer mainContainer() {
        return new MainContainer(cacheService());
    }
}
