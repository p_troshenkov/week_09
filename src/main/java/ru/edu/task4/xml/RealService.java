package ru.edu.task4.xml;

/**
 * ReadOnly
 */
public class RealService implements SomeInterface{
    @Override
    public String getName() {
        return "RealService";
    }
}
